#!/bin/bash
region=g
scale=Kf180/4.5i
file=station_map

gmt pscoast -R$region -J$scale -Ba30 -BWens -Swhite -G#CC9933 -A10000 -K > $file.ps

./gc_spread -17.8 -178 10 30 90 1 | awk '{print $2,$1}' | \
     gmt psxy  -R$region -J$scale -Si0.08c -Gblack -K  -O >> $file.ps
./az_spread 36.8 142.3 30 30 90 5 | awk '{print $2,$1}' | \
     gmt psxy  -R$region -J$scale -Si0.08c -Gred   -K  -O >> $file.ps

evince $file.ps &> /dev/null


